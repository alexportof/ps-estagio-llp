# Instruções de Utilizacao

## Instalação

*O framework escolhido pra ser usado foi o Vue.js e tambem o Quasar,framework todo baseado em Vue.js logo é necessario a instalação do Quasar*
### Os comandos a seguir sao rodados no Terminal

1. npm install -g vue-cli
2. npm install -g quasar-cli


*Apos instalar o quasar e o Vue.js é necessario entrar na pasta do projeto e rodar o seguinte comando*


3. quasar dev


 *a apicaçao irá subir e estará pronta para ser testada!*


## Utilização

*A tela inicial é a tela de login,caso o usuario ja tenha login,pode usr sua senha e seu emai e fazer login*

*Caso contrario,é necessario criar uma conta em 'Cadastrar'*

*Apos cadastro efetuado com email e senhas validas,será redirecionado para a tela de login,onde poderá realiza-lo tranquilamente*

*Depois de fazer o login,será redirecionado para a pagina home onde esta sendo mostrada a frase*

*Caso queira fazer logout,no canto superior esquerdo,tem o botao de 'Sair' e sendo clicado,redirecionará para a pagina de login*

