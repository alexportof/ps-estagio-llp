import Vue from 'vue';
import VueRouter from 'vue-router';
import auth from '../services/auth';

import routes from './routes';

Vue.use(VueRouter);

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  });

  Router.beforeEach((to, from, next) => {
    const token = auth.checkToken();

    if (to.path === '/') {
      if (token && token !== '') {
        return next();
      }

      return next({ path: '/login' });
    }

    if (to.path === '/login' || to.path === '/signup') {
      if (token && token !== '') {
        return next({ path: '/' });
      }

      return next();
    }

    return next();
  });

  return Router;
}
