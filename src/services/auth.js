import { LocalStorage } from 'quasar';

const auth = {};

auth.checkToken = () => LocalStorage.get.item('llp-token');

auth.setToken = token => LocalStorage.set('llp-token', token);

auth.logout = () => LocalStorage.set('llp-token', '');

export default auth;
