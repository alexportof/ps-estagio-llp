import axios from 'axios';
import auth from '../../services/auth';

const actions = {};

actions.createUser = (_, user) => {
  const url = 'https://testeestagio.llpdigital.com.br/api/auth/register';
  const requestConfig = {
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  };

  return axios.post(url, user, requestConfig);
};

actions.login = (_, user) => {
  const url = 'https://testeestagio.llpdigital.com.br/api/auth/login';
  const requestConfig = {
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  };

  return axios.post(url, user, requestConfig).then((response) => {
    auth.setToken(response.data.token);
  });
};

actions.getMessage = ({ commit }) => {
  const url = 'https://testeestagio.llpdigital.com.br/api/auth/llp';
  const requestConfig = {
    headers: { Accept: 'application/json', Authorization: `Bearer ${auth.checkToken()}` },
  };

  return axios.get(url, requestConfig).then((response) => {
    commit('setMessage', response.data.message);
  });
};

export default actions;
