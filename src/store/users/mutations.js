const mutations = {};

mutations.setMessage = (state, message) => {
  state.message = message;
};

export default mutations;
